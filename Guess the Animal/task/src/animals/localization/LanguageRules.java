package animals.localization;

import animals.ui.UserInterface;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public enum LanguageRules {
    EN {
        @Override
        public String processAnimal(String data) {
            if (data.matches("an? .+")) {
                return data;
            }
            final var article = "aeiou".indexOf(data.charAt(0)) == -1 ? "a" : "an";
            return article + " " + data;
        }
    },
    EO, LV, SV, RU;

    public static final String BASE_NAME = "animals";
    public static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BASE_NAME);
    public static final UserInterface UI = new UserInterface(RESOURCE_BUNDLE);
    private static final Pattern PATTERN_ANY = Pattern.compile(".*");
    private static final Logger log = Logger.getLogger(LanguageRules.class.getName());
    public static LanguageRules LOCAL;

    static {
        final var language = System.getProperty("user.language", "en").toUpperCase();
        log.config("System language is " + language);

        LOCAL = Arrays.stream(values())
                .filter(e -> language.startsWith(e.name()))
                .findFirst()
                .orElse(EN);

        log.config("Application language set to " + LOCAL.name());
    }

    public final ResourceBundle resourceBundle = ResourceBundle.getBundle(
            BASE_NAME, new Locale(this.name().toLowerCase()));
    private final Map<String, Pattern> patterns = new HashMap<>();

    {
        final var log = Logger.getLogger(LanguageRules.class.getName());
        log.log(Level.FINER, "Loading patterns for language: {0}", resourceBundle.getLocale());

        resourceBundle.keySet().stream()
                .filter(key -> key.endsWith(".pattern"))
                .forEach(key -> patterns.put(
                        key.replaceFirst("^(.+)\\.pattern$", "$1"),
                        Pattern.compile(resourceBundle.getString(key))));

        log.config(patterns::toString);
    }

    public static String capitalize(final String data) {
        return data.substring(0, 1).toUpperCase() + data.substring(1).toLowerCase();
    }

    public String processAnimal(final String data) {
        return data;
    }

    public String processStatement(final String data) {
        return data.replaceFirst("(.+)[.?]", "$1");
    }

    public String toAnimalName(final String animal) {
        return applyRules("toAnimalName", animal);
    }

    public String toDefiniteArticle(final String animal) {
        return applyRules("toDefiniteArticle", animal);
    }

    public String toAnimalFact(final String fact, final String animal) {
        return capitalize(String.format(
                applyRules("toAnimalFact", fact),
                toDefiniteArticle(animal))) + ".";
    }

    public String toNegative(final String statement) {
        return applyRules("toNegative", statement);
    }

    public String toStatementQuestion(String data) {
        return applyRules("toStatementQuestion", data);
    }

    public String toAnimalQuestion(String animal) {
        return applyRules("toAnimalQuestion", animal);
    }

    public boolean isPositiveAnswer(String answer) {
        return patterns
                .get("isPositiveAnswer")
                .matcher(answer)
                .matches();
    }

    public boolean isNegativeAnswer(String answer) {
        return patterns
                .get("isNegativeAnswer")
                .matcher(answer)
                .matches();
    }

    public boolean isCorrectAnimal(String data) {
        return patterns
                .getOrDefault("isAnimal", PATTERN_ANY)
                .matcher(data)
                .matches();
    }

    public boolean isCorrectStatement(String data) {
        return patterns
                .getOrDefault("isStatement", PATTERN_ANY)
                .matcher(data)
                .matches();
    }

    public String applyRules(final String rules, final String data) {
        for (int i = 1; ; i++) {
            final var key = rules + "." + i;
            final var rule = patterns.get(key);
            if (rule == null) {
                return data;
            }
            if (rule.matcher(data).matches()) {
                return data.replaceFirst(
                        resourceBundle.getString(key + ".find"),
                        resourceBundle.getString(key + ".replace"));
            }
        }
    }

}
