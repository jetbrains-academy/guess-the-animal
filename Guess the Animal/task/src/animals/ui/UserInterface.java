package animals.ui;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static animals.localization.LanguageRules.LOCAL;
import static java.util.Objects.isNull;

public class UserInterface {
    private static final Logger log = Logger.getLogger(UserInterface.class.getName());
    private static final Pattern MESSAGE_DELIMITER = Pattern.compile("\\f");
    private static final Scanner scanner = new Scanner(System.in);
    private static final Random random = new Random();
    private final ResourceBundle resourceBundle;

    public UserInterface(final ResourceBundle bundle) {
        this.resourceBundle = bundle;
    }

    private static String pickMessage(final String[] messages) {
        return messages[random.nextInt(messages.length)];
    }

    public void println(final String key, final Object... args) {
        System.out.println(MessageFormat.format(getText(key), args));
    }

    public void print(final String key, final Object... args) {
        System.out.print(MessageFormat.format(getText(key), args));
    }

    public void printf(final String key, final Object... args) {
        System.out.printf(resourceBundle.getString(key), args);
    }

    private String getText(final String key) {
        if (isNull(resourceBundle) || !resourceBundle.containsKey(key)) {
            return key;
        }
        if (resourceBundle.getObject(key) instanceof String[]) {
            return pickMessage(resourceBundle.getStringArray(key));
        }
        return pickMessage(MESSAGE_DELIMITER.split(resourceBundle.getString(key)));
    }

    public void printConditional(final String messageName) {
        log.entering(UserInterface.class.getName(), messageName);
        final var messages = new ArrayList<String>();
        final var time = LocalTime.now();
        final var date = LocalDate.now();

        final Map<String, Predicate<String>> conditions = Map.of(
                "time.after", startTime -> time.isAfter(LocalTime.parse(startTime)),
                "time.before", endTime -> time.isBefore(LocalTime.parse(endTime)),
                "date.after", startDate -> date.isAfter(LocalDate.parse(date.getYear() + "-" + startDate)),
                "date.before", endDate -> date.isBefore(LocalDate.parse(date.getYear() + "-" + endDate)),
                "date.equals", someDate -> date.equals(LocalDate.parse(date.getYear() + "-" + someDate)));

        final Predicate<String> isConditionalMessage = key -> key.startsWith(messageName + ".");

        final Predicate<String> isConditionActual = message -> {
            final var period = message.substring(messageName.length() + 1) + ".";
            return conditions.entrySet().stream()
                    .filter(entry -> resourceBundle.containsKey(period + entry.getKey()))
                    .allMatch(entry -> entry.getValue().test(resourceBundle.getString(period + entry.getKey())));
        };
        final Function<String, List<String>> splitMessage = key -> Arrays.asList(
                MESSAGE_DELIMITER.split(resourceBundle.getString(key)));

        resourceBundle.keySet().stream()
                .filter(isConditionalMessage)
                .filter(isConditionActual)
                .map(splitMessage)
                .forEach(messages::addAll);

        if (resourceBundle.containsKey(messageName)) {
            messages.addAll(splitMessage.apply(messageName));
        }
        System.out.println(pickMessage(messages.toArray(String[]::new)));
        log.exiting(UserInterface.class.getName(), messageName, messages);
    }

    public void pause() {
        println("game.enter");
        scanner.nextLine();
    }

    public String askAnimal(final String prompt) {
        while (true) {
            println(prompt);
            final var data = scanner.nextLine().toLowerCase().trim();
            if (LOCAL.isCorrectAnimal(data)) {
                return LOCAL.processAnimal(data);
            }
            println("animal.error");
        }
    }

    public String askStatement(final String first, final String second) {
        while (true) {
            println("game.specifyFact", first, second);
            println("game.statement");

            final var response = scanner.nextLine().toLowerCase().trim();
            if (LOCAL.isCorrectStatement(response)) {
                return LOCAL.processStatement(response);
            }
            println("game.example");
        }
    }

    public boolean askYesNo(final String key, final Object... args) {
        println(key, args);
        while (true) {
            final var answer = scanner.nextLine().toLowerCase().trim();
            if (LOCAL.isPositiveAnswer(answer)) {
                return true;
            }
            if (LOCAL.isNegativeAnswer(answer)) {
                return false;
            }
            println("ask.again");
        }
    }

}
