package animals;

import animals.services.Application;

import java.util.logging.Logger;

public final class Main {
    private static final Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        log.entering(Main.class.getName(), "main", args);
        final var isTypeSpecified = args.length > 1 && args[0].equals("-type");
        final var fileType = isTypeSpecified ? args[1] : "json";
        log.entering(Application.class.getName(), "run", fileType);

        new Application(fileType).run();

        log.exiting(Application.class.getName(), "run");
        log.exiting(Main.class.getName(), "main");
    }

}
