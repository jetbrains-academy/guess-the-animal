package animals.repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import static animals.localization.LanguageRules.LOCAL;
import static animals.localization.LanguageRules.capitalize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class Node {
    private String data;
    private Integer right;
    private Integer left;

    Node() {

    }

    public Node(String animal) {
        data = animal;
    }

    public Node(String statement, int right) {
        data = statement;
        this.right = right;
        this.left = right + 1;
    }

    @JsonIgnore
    public boolean isStatement() {
        return !isAnimal();
    }

    @JsonIgnore
    public boolean isAnimal() {
        return right == null;
    }

    @JsonIgnore
    public String getQuestion() {
        return capitalize(isAnimal()
                ? LOCAL.toAnimalQuestion(data)
                : LOCAL.toStatementQuestion(data));
    }

    public String getData() {
        return data;
    }

    public Integer getRight() {
        return right;
    }

    public Integer getLeft() {
        return left;
    }

}
