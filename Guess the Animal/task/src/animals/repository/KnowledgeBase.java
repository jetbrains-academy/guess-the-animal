package animals.repository;

import java.util.*;

import static animals.localization.LanguageRules.LOCAL;
import static animals.localization.LanguageRules.capitalize;
import static java.util.stream.Collectors.summarizingInt;

public class KnowledgeBase {
    private final Map<String, List<String>> animals = new TreeMap<>();
    private final List<Node> tree = new ArrayList<>();
    private int currentNode;

    public boolean isEmpty() {
        return tree.isEmpty();
    }

    public List<Node> getNodes() {
        return Collections.unmodifiableList(tree);
    }

    public void setNodes(final Node[] nodes) {
        tree.clear();
        tree.addAll(Arrays.asList(nodes));
    }

    public Node getRoot() {
        return tree.get(0);
    }

    public void setRoot(final Node root) {
        tree.clear();
        tree.add(root);
    }

    public int size() {
        return tree.size();
    }

    public Node current() {
        return tree.get(currentNode);
    }

    public void next(final boolean isYes) {
        currentNode = isYes ? current().getRight() : current().getLeft();
    }

    public void goRoot() {
        currentNode = 0;
    }

    public void add(final Node newAnimal, final String statement, final boolean isCorrect) {
        final var oldAnimal = current();

        tree.set(currentNode, new Node(statement, tree.size()));
        tree.add(isCorrect ? newAnimal : oldAnimal);
        tree.add(isCorrect ? oldAnimal : newAnimal);
    }

    public Node getNode(final int index) {
        return tree.get(index);
    }

    public Map<String, List<String>> getAnimals() {
        final var isChanged = tree.size() != animals.size() * 2 - 1;
        if (isChanged) {
            animals.clear();
            collectAnimals(0, new LinkedList<>());
        }
        return Collections.unmodifiableMap(animals);
    }

    private void collectAnimals(final int index, final Deque<String> facts) {
        final var node = tree.get(index);
        if (node.isAnimal()) {
            animals.put(LOCAL.toAnimalName(node.getData()), List.copyOf(facts));
            return;
        }
        final var statement = node.getData();
        facts.add(capitalize(statement) + ".");
        collectAnimals(node.getRight(), facts);
        facts.removeLast();
        facts.add(capitalize(LOCAL.toNegative(statement)) + ".");
        collectAnimals(node.getLeft(), facts);
        facts.removeLast();
    }

    public IntSummaryStatistics getStatistics() {
        return getAnimals().values().stream().collect(summarizingInt(List::size));
    }

}
