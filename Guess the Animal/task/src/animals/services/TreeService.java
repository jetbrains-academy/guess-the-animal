package animals.services;

import animals.repository.KnowledgeBase;

import static animals.localization.LanguageRules.LOCAL;
import static animals.localization.LanguageRules.UI;
import static java.util.Collections.emptyList;

public class TreeService {
    private final KnowledgeBase repository;

    public TreeService(KnowledgeBase repository) {
        this.repository = repository;
    }

    public void list() {
        UI.println("tree.list.animals");
        repository.getAnimals().forEach((animal, facts) -> UI.printf("tree.list.printf", animal, facts.size()));
    }

    public void search() {
        final var animal = LOCAL.toAnimalName(UI.askAnimal("tree.search.animal"));
        final var facts = repository.getAnimals().getOrDefault(animal, emptyList());
        UI.println(facts.isEmpty() ? "tree.search.noFacts" : "tree.search.facts", animal);
        facts.forEach(fact -> UI.printf("tree.search.printf", fact));
    }

    public void statistics() {
        final var stats = repository.getStatistics();
        UI.println("tree.stats.title");
        UI.println("tree.stats.root", repository.getRoot().getData());
        UI.println("tree.stats.nodes", stats.getCount() * 2 - 1);
        UI.println("tree.stats.animals", stats.getCount());
        UI.println("tree.stats.statements", stats.getCount() - 1);
        UI.println("tree.stats.height", stats.getMax());
        UI.println("tree.stats.minimum", stats.getMin());
        UI.println("tree.stats.average", stats.getAverage());
    }

    public void print() {
        printNode(0, false, " ");
    }

    private void printNode(final int index, final boolean isYes, String prefix) {
        final var node = repository.getNode(index);
        UI.printf("tree.print.printf", prefix, isYes ? '├' : '└',
                node.isAnimal() ? node.getData() : node.getQuestion());
        if (node.isStatement()) {
            prefix += isYes ? '│' : ' ';
            printNode(node.getRight(), true, prefix);
            printNode(node.getLeft(), false, prefix);
        }
    }
}
