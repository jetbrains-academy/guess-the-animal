package animals.services;

import animals.repository.FileType;
import animals.repository.KnowledgeBase;
import animals.repository.Node;
import animals.ui.Menu;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import static animals.localization.LanguageRules.*;

public final class Application implements Runnable {
    private static final Logger log = Logger.getLogger(Application.class.getName());
    private final KnowledgeBase repository = new KnowledgeBase();

    private final File database;
    private final ObjectMapper mapper;

    public Application(String fileType) {
        final var fileName = BASE_NAME + (
                LOCAL == EN ? "" : "_" + LOCAL.name().toLowerCase()
        );
        database = new File(fileName + "." + fileType);
        mapper = FileType
                .valueOf(fileType.toUpperCase())
                .getObjectMapper();
    }

    @Override
    public void run() {
        UI.printConditional("greeting");

        loadRepository();
        checkRepository();
        showApplicationMenu();
        saveRepository();

        UI.printConditional("farewell");
    }

    private void showApplicationMenu() {
        UI.println("welcome");
        final var treeService = new TreeService(repository);

        Menu.create(LOCAL.resourceBundle)
                .add("menu.entry.play", new Game(repository))
                .add("menu.entry.list", treeService::list)
                .add("menu.entry.search", treeService::search)
                .add("menu.entry.statistics", treeService::statistics)
                .add("menu.entry.print", treeService::print)
                .addExit()
                .run();
    }

    private void checkRepository() {
        log.entering(Application.class.getName(), "checkRepository", repository.size());
        if (repository.isEmpty()) {
            log.config("The repository is empty. The user will be asked about a favorite animal.");
            UI.println("animal.wantLearn");
            repository.setRoot(
                    new Node(UI.askAnimal("animal.askFavorite"))
            );
            UI.print("animal.nice");
            UI.println("animal.learnedMuch");
        }
        log.exiting(Application.class.getName(), "checkRepository", repository.size());
    }

    public void loadRepository() {
        log.entering(Application.class.getName(), "loadRepository", database.getAbsolutePath());
        try {
            repository.setNodes(mapper.readValue(database, Node[].class));
        } catch (IOException error) {
            log.warning(error.getMessage());
        }
        log.exiting(Application.class.getName(), "loadRepository", repository.size() + " records");
    }

    private void saveRepository() {
        log.entering(Application.class.getName(), "saveRepository", database.getAbsolutePath());
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(database, repository.getNodes());
        } catch (IOException error) {
            log.warning(error.getMessage());
        }
        log.exiting(Application.class.getName(), "saveRepository",
                "Knowledge Base saved successfully.");
    }

}
