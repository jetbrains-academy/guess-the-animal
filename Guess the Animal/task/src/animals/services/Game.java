package animals.services;

import animals.repository.KnowledgeBase;
import animals.repository.Node;

import java.util.logging.Level;
import java.util.logging.Logger;

import static animals.localization.LanguageRules.*;

public final class Game implements Runnable {
    private static final Logger log = Logger.getLogger(Game.class.getName());

    private final KnowledgeBase repository;

    public Game(KnowledgeBase repository) {
        this.repository = repository;
    }

    @Override
    public void run() {
        UI.println("game.letsPlay");

        do play();
        while (UI.askYesNo("game.again"));
    }

    private void play() {
        log.entering(Game.class.getName(), "play");
        UI.println("game.think");
        UI.pause();

        repository.goRoot();

        while (repository.current().isStatement()) {
            repository.next(
                    UI.askYesNo(repository.current().getQuestion())
            );
        }

        final var myGuess = repository.current();

        if (UI.askYesNo(myGuess.getQuestion())) {
            UI.println("game.thanks");
            log.exiting(Game.class.getName(), "play", "The computer guessed the animal correctly.");
            return;
        }

        final var youAnimal = new Node(UI.askAnimal("game.giveUp"));
        final var positive = UI.askStatement(myGuess.getData(), youAnimal.getData());
        final var negative = LOCAL.toNegative(positive);
        final var isCorrect = UI.askYesNo("game.isCorrect", youAnimal.getData());

        repository.add(youAnimal, positive, isCorrect);

        final var fact1 = isCorrect ? negative : positive;
        final var fact2 = isCorrect ? positive : negative;

        UI.println("game.learned");
        UI.println(" - {0}", capitalize(LOCAL.toAnimalFact(fact1, myGuess.getData())));
        UI.println(" - {0}", capitalize(LOCAL.toAnimalFact(fact2, youAnimal.getData())));
        UI.println("game.distinguish");
        UI.println(" - {0}", capitalize(repository.current().getQuestion()));

        UI.print("animal.nice");
        UI.println("animal.learnedMuch");
        log.log(Level.CONFIG, "The computer assumed {0}, but the user conceived of {1}",
                new String[]{myGuess.getData(), youAnimal.getData()});
        log.exiting(Game.class.getName(), "play", "The computer learned about the new animal.");
    }

}
